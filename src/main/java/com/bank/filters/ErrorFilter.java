package com.bank.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/*")
public class ErrorFilter implements Filter {
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		try {
			boolean ignoreAuth = true;
			HttpServletRequest req = (HttpServletRequest) request;
			HttpServletResponse res = (HttpServletResponse) response;
			HttpSession session = req.getSession(false);
			if (session == null) {
				if ((req.getServletPath().contains("/api")) || (req.getServletPath().contains(".jsp"))
						|| isAsset(req)) {
					if (ignoreAuth || req.getServletPath().contains("/api/user/login") || req.getServletPath().contains(".jsp")
							|| isAsset(req)) {
						chain.doFilter(request, response);
					} else {
						req.getRequestDispatcher("/index.jsp").forward(req, res);
					}
				} else {
					req.getRequestDispatcher("/index.jsp").forward(req, res);
				}
			} else if (ignoreAuth || req.getServletPath().contains("/api") || isAsset(req)
					|| req.getServletPath().contains("/api/user/login") || req.getServletPath().contains(".jsp")) {
				chain.doFilter(request, response);
			} else {
				req.getRequestDispatcher("/index.jsp").forward(req, res);
			}
		} catch (Exception e) {
			System.out.println(e);
			throw e;
		}
	}

	private boolean isAsset(HttpServletRequest req) {
		return (req.getServletPath().contains(".css")
				|| (req.getServletPath().contains(".js") && !req.getServletPath().contains(".jsp")));
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void destroy() {
	}
}
