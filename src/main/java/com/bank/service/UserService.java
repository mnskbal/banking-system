package com.bank.service;

import org.springframework.stereotype.Service;

import com.bank.exception.BankingException;
import com.bank.model.dto.UserDto;
import com.bank.model.entity.User;
import com.bank.repository.UserRepository;

@Service
public class UserService {

	private UserRepository userRepository = new UserRepository();

	public UserDto getUser(Long userId) throws BankingException {
		User user = userRepository.getUser(userId);
		if (user == null)
			throw new BankingException("Unable to find any users with the given identifier!");
		return new UserDto(user);
	}
}
