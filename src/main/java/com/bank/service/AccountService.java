package com.bank.service;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import com.bank.enums.ErrorCode;
import com.bank.enums.TransactionType;
import com.bank.exception.BankingException;
import com.bank.model.dto.AccountDto;
import com.bank.model.dto.BankDto;
import com.bank.model.dto.TransactionDto;
import com.bank.model.entity.Account;
import com.bank.model.entity.Bank;
import com.bank.repository.AccountRepository;
import com.bank.repository.BankRepository;

@Service
public class AccountService {

	private AccountRepository accountRepository = new AccountRepository();
	private BankRepository bankRepository = new BankRepository();
	private TransactionService transactionService = new TransactionService();

	public BigDecimal enquireBalance(Long accountId) throws BankingException {
		return getAccount(accountId).getBalance();
	}

	public boolean withdraw(Long accountId, BigDecimal amount) throws BankingException {
		boolean status = false;
		Account acc = getAccount(accountId);
		if (acc.getBalance().compareTo(amount) > 0) {
			status = accountRepository.updateAccountBalance(accountId, acc.getBalance().subtract(amount))
					&& transactionService.createTransaction(accountId, amount, TransactionType.D);
		} else {
			throw new BankingException(ErrorCode.INSUFFICIENT_BALANCE);
		}
		return status;
	}

	public boolean deposit(Long accountId, BigDecimal amount) throws BankingException {
		Account acc = getAccount(accountId);
		return accountRepository.updateAccountBalance(accountId, acc.getBalance().add(amount))
				&& transactionService.createTransaction(accountId, amount, TransactionType.C);
	}

	public Account getAccount(Long accountId) throws BankingException {
		Account acc = accountRepository.getAccount(accountId);
		if (acc == null)
			throw new BankingException(ErrorCode.ACCOUNT_NOT_FOUND);
		return acc;
	}

	public List<AccountDto> getAccounts(Long userId) throws BankingException {
		List<AccountDto> accountDtos = Collections.emptyList();
		List<Account> accounts = accountRepository.listAccounts(userId);
		if (CollectionUtils.isNotEmpty(accounts)) {
			accountDtos = accounts.stream().filter(Objects::nonNull).map(AccountDto::new).map(a -> {
				try {
					BankDto bankDto = a.getBank();
					if (bankDto == null) {
						Bank bank = bankRepository.getBank(a.getBankId());
						if (bank != null)
							a.setBank(new BankDto(bank));
					}
					if (CollectionUtils.isEmpty(a.getTransactions())) {
						Set<TransactionDto> transactionDtos = transactionService.fetchAllTransactionDtos(a.getId());
						if (CollectionUtils.isNotEmpty(transactionDtos))
							a.setTransactions(transactionDtos);
					}
				} catch (BankingException e) {
					e.printStackTrace();
				}
				return a;
			}).filter(Objects::nonNull).collect(Collectors.toList());
		}
		return accountDtos;
	}

}
