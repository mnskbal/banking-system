package com.bank.service;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import com.bank.enums.TransactionType;
import com.bank.exception.BankingException;
import com.bank.model.dto.TransactionDto;
import com.bank.model.entity.Transaction;
import com.bank.repository.TransactionRepository;

@Service
public class TransactionService {

	private TransactionRepository transactionRepository = new TransactionRepository();

	public Set<TransactionDto> fetchAllTransactionDtos(Long accountId) throws BankingException {
		Set<Transaction> transactions = fetchAllTransactions(accountId);
		return CollectionUtils.isNotEmpty(transactions)
				? transactions.stream().filter(Objects::nonNull).map(TransactionDto::new).collect(Collectors.toSet())
				: Collections.emptySet();
	}

	public Set<Transaction> fetchAllTransactions(Long accountId) throws BankingException {
		return transactionRepository.findAllTransactions(accountId);
	}

	public boolean createTransaction(Long accountId, BigDecimal amount, TransactionType type) throws BankingException {
		TransactionDto transactionDto = new TransactionDto(amount, type, new Date(), accountId);
		return transactionRepository.createTransaction(accountId, transactionDto);
	}

}
