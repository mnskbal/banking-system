package com.bank.model.entity;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//ibatis --> mybatis
@Entity
@Table(name = "account")
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;

	private BigDecimal balance;

	@Column(name = "bank_id")
	private Long bankId;

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = Bank.class)
	@JoinColumn(name = "bank_id", insertable = false, updatable = false)
	private Bank bank;

	@Column(name = "user_id")
	private Long userId;

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = User.class)
	@JoinColumn(name = "user_id", insertable = false, updatable = false)
	private User user;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "accountId", targetEntity = Transaction.class, fetch = FetchType.EAGER)
	private Set<Transaction> transactions;

	public Account() {
		super();
	}

	public Account(Long id, BigDecimal balance, Long bankId, Long userId) {
		super();
		this.id = id;
		this.balance = balance;
		this.bankId = bankId;
		this.userId = userId;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public Long getBankId() {
		return bankId;
	}

	public void setBankId(Long bankId) {
		this.bankId = bankId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Set<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(Set<Transaction> transactions) {
		this.transactions = transactions;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Bank getBank() {
		return bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		Account o = (Account) obj;
		return this.id.equals(o.getId());
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", balance=" + balance + ", bankId=" + bankId + ", userId=" + userId + "]";
	}

}
