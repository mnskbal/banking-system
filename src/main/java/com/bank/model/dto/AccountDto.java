package com.bank.model.dto;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.bank.model.entity.Account;

public class AccountDto {

	private Long id;
	private BigDecimal balance;
	private Long bankId;
	private Long userId;
	private Set<TransactionDto> transactions;

	// transiant variables
	private BankDto bank;

	public AccountDto() {
		super();
	}

	public AccountDto(Long id, BigDecimal balance, Long bankId, Long userId) {
		super();
		this.id = id;
		this.balance = balance;
		this.bankId = bankId;
		this.userId = userId;
	}

	public AccountDto(Account account) {
		super();
		this.id = account.getId();
		this.balance = account.getBalance();
		this.bankId = account.getBankId();
		this.userId = account.getUserId();
		if (CollectionUtils.isNotEmpty(account.getTransactions()))
			this.transactions = account.getTransactions().stream().filter(Objects::nonNull).map(TransactionDto::new)
					.collect(Collectors.toSet());
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public Long getBankId() {
		return bankId;
	}

	public void setBankId(Long bankId) {
		this.bankId = bankId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Set<TransactionDto> getTransactions() {
		return transactions;
	}

	public void setTransactions(Set<TransactionDto> transactions) {
		this.transactions = transactions;
	}

	public Long getId() {
		return id;
	}

	public BankDto getBank() {
		return bank;
	}

	public void setBank(BankDto bank) {
		this.bank = bank;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		AccountDto o = (AccountDto) obj;
		return this.id.equals(o.getId());
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", balance=" + balance + ", bankId=" + bankId + ", userId=" + userId + "]";
	}

}
