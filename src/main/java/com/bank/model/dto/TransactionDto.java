package com.bank.model.dto;

import java.math.BigDecimal;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Objects;

import com.bank.enums.TransactionType;
import com.bank.model.entity.Transaction;

public class TransactionDto {

	private Long id;
	private BigDecimal amount;
	private TransactionType type;
	private Date date;
	private Long accountId;

	public TransactionDto() {
		super();
	}

	public TransactionDto(BigDecimal amount, TransactionType type, Date date, Long accountId) {
		super();
		this.amount = amount;
		this.type = type;
		this.date = date;
		this.accountId = accountId;
	}

	public TransactionDto(Long id, BigDecimal amount, TransactionType type, Date date, Long accountId) {
		super();
		this.id = id;
		this.amount = amount;
		this.type = type;
		this.date = date;
		this.accountId = accountId;
	}

	public TransactionDto(Transaction transaction) {
		super();
		this.id = transaction.getId();
		this.amount = transaction.getAmount();
		this.type = transaction.getType();
		this.date = new Date(transaction.getDate().toInstant(ZoneOffset.UTC).toEpochMilli());
		this.accountId = transaction.getAccountId();
	}

	public Long getId() {
		return id;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public TransactionType getType() {
		return type;
	}

	public void setType(TransactionType type) {
		this.type = type;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		TransactionDto o = (TransactionDto) obj;
		return this.id.equals(o.getId());
	}

	@Override
	public String toString() {
		return "Transaction [id=" + id + ", amount=" + amount + ", type=" + type + ", date=" + date + ", accountId="
				+ accountId + "]";
	}

}
