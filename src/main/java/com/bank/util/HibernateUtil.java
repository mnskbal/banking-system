package com.bank.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	private HibernateUtil() {
	}

	private static final SessionFactory SESSION_FACTORY = buildSessionFactory();

	private static SessionFactory buildSessionFactory() {
		try {
			return new Configuration().configure().buildSessionFactory();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to establish connection with the database");
		}
	}

	public static SessionFactory getSessionFactory() {
		return SESSION_FACTORY;
	}

	public static Session openSession() {
		return SESSION_FACTORY.openSession();
	}

	public static void closeSession(Session session) {
		session.close();
	}

	public static void closeSessionFactory() {
		SESSION_FACTORY.close();
	}

}
