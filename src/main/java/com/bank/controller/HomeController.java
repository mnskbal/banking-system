package com.bank.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.bank.model.dto.UserDto;
import com.bank.service.UserService;

@RestController
@RequestMapping(value = "/api/user")
public class HomeController {

	@Autowired
	private UserService userService;

	@GetMapping(value = "/login", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ModelAndView getLogin(HttpServletRequest req, @RequestParam(name = "uId", required = false) Long uId,
			@RequestParam(name = "pwd", required = false) String cred, Model model) {
		return login(req, uId, cred, model);
	}

	@PostMapping(value = "/login", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ModelAndView login(HttpServletRequest req, @RequestParam(name = "uId", required = false) Long uId,
			@RequestParam(name = "pwd", required = false) String cred, Model model) {
		String destination = "index";
		try {
			HttpSession session = req.getSession(false);
			if (session != null) {
				uId = session.getAttribute("uId") != null ? Long.valueOf(session.getAttribute("uId").toString()) : null;
				if ((uId != null && uId > 0)) {
					UserDto user = userService.getUser(uId);
					model.addAttribute("uName", user.getLastName() + ", " + user.getFirstName());
					model.addAttribute("accounts", new ArrayList<>(user.getAccounts()));
					destination = "home";
				}
			} else {
				if ((uId != null && uId > 0) && StringUtils.isNotBlank(cred)) {
					UserDto user = userService.getUser(uId);
					if (user.getPassword().equals(cred)) {
						model.addAttribute("uName", user.getLastName() + ", " + user.getFirstName());
						model.addAttribute("accounts", new ArrayList<>(user.getAccounts()));
						session = req.getSession();
						session.setAttribute("uId", user.getId());
						destination = "home";
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView(destination);
	}

	@GetMapping(value = "/logout")
	public ModelAndView doGet(HttpServletRequest req) {
		HttpSession session = req.getSession(false);
		if (session != null)
			session.invalidate();
		return new ModelAndView("index");
	}

}
