package com.bank.controller;

import java.util.Objects;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.bank.model.dto.AccountDto;
import com.bank.model.dto.UserDto;
import com.bank.service.AccountService;
import com.bank.service.UserService;

@RestController
@RequestMapping(value = "/api")
public class NavigationController {

	@Autowired
	private AccountService accountService;

	@Autowired
	private UserService userService;

	@GetMapping(value = "/navigate")
	public ModelAndView navigate(HttpServletRequest req, @RequestParam(name = "action") String action) {
		return processRequest(req, action);
	}

	@PostMapping(value = "/navigate")
	public ModelAndView doNavigate(HttpServletRequest req, @RequestParam(name = "action") String action) {
		return processRequest(req, action);
	}

	private ModelAndView processRequest(HttpServletRequest req, String action) {
		String destination = "/api/user/login";
		try {
			HttpSession session = req.getSession(false);
			if (session != null && StringUtils.isNotBlank(action)) {
				Long uId = session.getAttribute("uId") != null ? Long.valueOf(session.getAttribute("uId").toString())
						: null;
				if ((uId != null && uId > 0)) {
					UserDto user = userService.getUser(uId);
					req.setAttribute("uName", user.getLastName() + ", " + user.getFirstName());
					req.setAttribute("accountIds", accountService.getAccounts(uId).stream().filter(Objects::nonNull)
							.map(AccountDto::getId).collect(Collectors.toList()));
					destination = "withdraw".equalsIgnoreCase(action) ? "withdraw" : "deposit";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView(destination);
	}

}
