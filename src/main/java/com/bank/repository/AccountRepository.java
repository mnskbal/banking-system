package com.bank.repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.bank.exception.BankingException;
import com.bank.model.dto.AccountDto;
import com.bank.model.entity.Account;
import com.bank.util.HibernateUtil;

@Repository
public class AccountRepository {

	public Account getAccount(Long accountId) throws BankingException {
		Account account = null;
		try {
			Session session = HibernateUtil.openSession();
			Query query = session.createQuery("FROM com.bank.model.entity.Account WHERE id = :accountId");
			query.setLong("accountId", accountId);
			account = (Account) query.uniqueResult();
			HibernateUtil.closeSession(session);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BankingException(e.getMessage());
		}
		return account;
	}

	@SuppressWarnings("unchecked")
	public List<Account> listAccounts(Long userId) throws BankingException {
		List<Account> accounts = new ArrayList<>();
		try {
			Session session = HibernateUtil.openSession();
			Query query = session.createQuery("FROM com.bank.model.entity.Account WHERE userId = :userId");
			query.setLong("userId", userId);
			accounts = query.list();
			HibernateUtil.closeSession(session);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BankingException(e.getMessage());
		}
		return accounts;
	}

	public boolean updateAccount(Long accountId, AccountDto accountDto) throws BankingException {
		boolean status = false;
		try {
			Session session = HibernateUtil.openSession();
			Account account = getAccount(accountId);
			account.setBalance(accountDto.getBalance());
			account.setBankId(accountDto.getBankId());
			account.setUserId(accountDto.getUserId());
			session.saveOrUpdate(account);
			status = true;
			HibernateUtil.closeSession(session);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BankingException(e.getMessage());
		}
		return status;
	}

	public boolean updateAccountBalance(Long accountId, BigDecimal balance) throws BankingException {
		boolean status = false;
		try {
			Session session = HibernateUtil.openSession();
			Account account = getAccount(accountId);
			account.setBalance(balance);
			session.saveOrUpdate(account);
			status = true;
			HibernateUtil.closeSession(session);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BankingException(e.getMessage());
		}
		return status;
	}
}
