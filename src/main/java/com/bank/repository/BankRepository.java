package com.bank.repository;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.bank.exception.BankingException;
import com.bank.model.entity.Bank;
import com.bank.util.HibernateUtil;

@Repository
public class BankRepository {

	public Bank getBank(Long bankId) throws BankingException {
		Bank bank = null;
		try {
			Session session = HibernateUtil.openSession();
			Query query = session.createQuery("FROM com.bank.model.entity.Bank WHERE id = :bankId");
			query.setLong("bankId", bankId);
			bank = (Bank) query.uniqueResult();
			HibernateUtil.closeSession(session);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BankingException(e.getMessage());
		}
		return bank;
	}

}
