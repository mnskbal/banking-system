package com.bank.repository;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.bank.exception.BankingException;
import com.bank.model.entity.User;
import com.bank.util.HibernateUtil;

//ORM - Object Relational Mapping framework
@Repository
public class UserRepository {

	public User getUser(Long userId) throws BankingException {
		User user = null;
		try {
			Session session = HibernateUtil.openSession();
			Query query = session.createQuery("FROM com.bank.model.entity.User WHERE id = :userId");
			query.setLong("userId", userId);
			user = (User) query.uniqueResult();
			HibernateUtil.closeSession(session);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BankingException(e.getMessage());
		}
		return user;
	}

}
