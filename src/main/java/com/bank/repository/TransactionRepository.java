package com.bank.repository;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.bank.exception.BankingException;
import com.bank.model.dto.TransactionDto;
import com.bank.model.entity.Transaction;
import com.bank.util.HibernateUtil;

@Repository
public class TransactionRepository {

	@SuppressWarnings("unchecked")
	public Set<Transaction> findAllTransactions(Long accountId) throws BankingException {
		Set<Transaction> transactions = new HashSet<>();
		try {
			Session session = HibernateUtil.openSession();
			Query query = session.createQuery("FROM com.bank.model.entity.Transaction WHERE accountId = :accountId");
			query.setLong("accountId", accountId);
			transactions.addAll(query.list());
			HibernateUtil.closeSession(session);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BankingException(e.getMessage());
		}
		return transactions;
	}

	public boolean createTransaction(Long accountId, TransactionDto transactionDto) throws BankingException {
		boolean status = false;
		try {
			Session session = HibernateUtil.openSession();
			Transaction transaction = new Transaction(transactionDto.getAmount(), transactionDto.getType(),
					transactionDto.getDate(), accountId);
			Serializable s = session.save(transaction);
			System.out.println(s);
			status = s != null;
			HibernateUtil.closeSession(session);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BankingException(e.getMessage());
		}
		return status;
	}
}
