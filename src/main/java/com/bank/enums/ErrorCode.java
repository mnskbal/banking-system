package com.bank.enums;

public enum ErrorCode {

	INSUFFICIENT_BALANCE(101, "Insufficient balance to perform this action!"),
	ACCOUNT_NOT_FOUND(102, "Oops... Unable to find the account!"), UNKNOWN(199, "Oops... Unable to find the account!");

	private Integer code;
	private String description;

	private ErrorCode(Integer code, String description) {
		this.code = code;
		this.description = description;
	}

	public Integer getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

}
