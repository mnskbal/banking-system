<%@ page language="java" contentType="text/html" %>
<%@ page session="false" %>
<!doctype html>
<html lang="en">
  <head>
  	<title>Online Banking System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
		
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
<style>
/*set border to the form*/
form {
	border: 3px solid #f1f1f1;
}
/*assign full width inputs*/
input[type=text], input[type=password] {
	width: 100%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 1px solid #ccc;
	box-sizing: border-box;
}
/*set a style for the buttons*/
button {
	background-color: #4CAF50;
	color: white;
	padding: 14px 20px;
	margin: 8px 0;
	border: none;
	cursor: pointer;
	width: 100%;
}
/* set a hover effect for the button*/
button:hover {
	opacity: 0.8;
}
/*set extra style for the cancel button*/
.cancelbtn {
	width: auto;
	padding: 10px 18px;
	background-color: #f44336;
}
/*centre the display image inside the container*/
.imgcontainer {
	text-align: center;
	margin: 24px 0 12px 0;
}
/*set image properties*/
img.avatar {
	width: 40%;
	border-radius: 50%;
}
/*set padding to the container*/
.container {
	padding: 16px;
}
/*set the forgot password text*/
span.psw {
	float: right;
	padding-top: 16px;
}
/*set styles for span and cancel button on small screens*/
@media screen and (max-width: 300px) {
	span.psw {
		display: block;
		float: none;
	}
	.cancelbtn {
		width: 100%;
	}
}
</style>		
  </head>
  <body>		
	<div class="wrapper d-flex align-items-stretch">		
      <div id="content" class="p-4 p-md-5">
		<%@include file="layouts/header.jsp" %>
			<center>
				<h2>Login Form</h2>
			</center>
			<form name="lform" method="post"
				action="/banking-system/api/user/login">
				<div class="container">
					<label><b>Username</b></label> <input type="text"
						placeholder="Enter User Id" name="uId" required> <label><b>Password</b></label>
					<input type="password" placeholder="Enter Password" name="pwd"
						required>
					<button type="submit">Login</button>
				</div>
			</form>
		</div>
      </div>
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/popper.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/main.js"></script>
  </body>
</html>