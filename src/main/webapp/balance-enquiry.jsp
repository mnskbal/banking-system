<%@ page language="java" contentType="text/html" import="java.math.BigDecimal" %>
<html>
<head>
<title>Balance Enquiry</title>
</head>
<body>
	<%! BigDecimal balance; %>
	<% balance = (BigDecimal)request.getAttribute("balance");%>
	<h1>Your Account balance is <%=balance%></h1>
</body>
</html>