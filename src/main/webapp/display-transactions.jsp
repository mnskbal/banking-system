<%@ page language="java" contentType="text/html"%> <%@ page
import="org.apache.commons.collections4.CollectionUtils" %> <%@ page
import="java.util.Objects" %> <%@ page
import="com.bank.model.dto.TransactionDto" %> <%@ page
import="java.util.Set" %>

<html>
<head>
<title>Transactions</title>
</head>
<body>
	<% Set
	<TransactionDto> transactions =
	(Set)request.getAttribute("transactions"); if
	(CollectionUtils.isNotEmpty(transactions)) { %>
	<table>
		<tr>
			<td>DATE</td>
			<td>TYPE</td>
			<td>AMOUNT</td>
		</tr>
		<% for(TransactionDto t:transactions) { %>
		<tr>
			<td><%=t.getDate()%></td>
			<td><%=t.getType()%></td>
			<td><%=t.getAmount()%></td>
		</tr>
		<% } %>
	</table>
	<% } else { %>
	<h1>Looks like you haven't made any transactions!</h1>
	<% }%> 
</body>
</html>
