<%@ page language="java" contentType="text/html" %> <%@ page
session="false" %> <%@ page import="java.util.Set" %> <%@ page
import="java.util.List" %> <%@ page
import="com.bank.model.dto.TransactionDto" %> <%@ page
import="com.bank.model.dto.AccountDto" %> <%@ page
import="com.bank.model.dto.BankDto" %>

<!doctype html>
<html lang="en">
<head>
<title>Online Banking System</title>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link
	href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900"
	rel="stylesheet">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/style.css">
<style type="text/css">
@import
	url('https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700');

$
base-spacing-unit: 24px ; $half-spacing-unit: $base-spacing-unit / 2 ; $color-alpha: #1772FF 
	 ; $color-form-highlight: #EEEEEE ; *, *:before, *:after {
	box-sizing: border-box;
}

body {
	padding: $base-spacing-unit;
	font-family: 'Source Sans Pro', sans-serif;
	margin: 0;
}

h1, h2, h3, h4, h5, h6 {
	margin: 0;
}

.container {
	margin-right: auto;
	margin-left: auto;
	display: flex;
	justify-content: center;
	align-items: center;
}

.table {
	width: 100%;
	border: 1px solid black;
}

.table-header {
	display: flex;
	width: 100%;
	background: #000;
	padding: ($half-spacing-unit* 1.5) 0;
}

.table-row {
	display: flex;
	width: 100%;
	padding: ($half-spacing-unit* 1.5) 0;
	&:
	nth-of-type
	(odd)
	{
	background
	:
	$
	color-form-highlight;
}

}
.table-data, .header__item {
	flex: 1 1 20%;
	text-align: center;
}

.header__item {
	text-transform: uppercase;
}

.filter__link {
	color: white;
	text-decoration: none;
	position: relative;
	display: inline-block;
	padding-left: $base-spacing-unit;
	padding-right: $base-spacing-unit; &:: after { content : '';
	position: absolute;
	right: -($half-spacing-unit* 1.5);
	color: white;
	font-size: $half-spacing-unit;
	top: 50%;
	transform: translateY(-50%);
}

&
.desc::after {
	content: '(desc)';
}

&
.asc::after {
	content: '(asc)';
}

}
.parent {
	text-align: center;
}

.child {
	display: inline-block;
	padding: 0rem 6rem;
	vertical-align: middle;
}
</style>
</head>
<body>
	<div class="wrapper d-flex align-items-stretch">
		<%@include file="layouts/left-nav-bar.jsp" %>
		<!-- Page Content  -->
		<div id="content" class="p-4 p-md-5">
			<%@include file="layouts/header.jsp" %> <% String userName =
			String.valueOf(request.getAttribute("uName")); List
			<AccountDto> accounts = (List<AccountDto>)request.getAttribute("accounts");
			%>
			<h2 class="mb-4">Welcome <%=userName%></h2>
			<p>Below is the summary of your accounts</p>
			<% if(accounts != null) { for(AccountDto account: accounts) { BankDto
			bank = account.getBank(); Set <TransactionDto>
			transactions = account.getTransactions(); %>
			<div class="container">
				<div class="table">
					<div class="parent">
						<% if(bank != null) { %>
						<div class="child">
							<p>Bank: <%=bank.getName()%></p>
						</div>
						<% } %>
						<div class="child">
							<p>Account# <%=account.getId()%></p>
						</div>
						<div class="child">
							<p>Balance: <%=account.getBalance()%></p>
						</div>
						<br>
					</div>
					<div class="table-header">
						<div class="header__item">
							<a id="name" class="filter__link" href="#">ID</a>
						</div>
						<div class="header__item">
							<a id="wins" class="filter__link filter__link--number" href="#">Amount</a>
						</div>
						<div class="header__item">
							<a id="losses" class="filter__link filter__link--number" href="#">Type</a>
						</div>
						<div class="header__item">
							<a id="total" class="filter__link filter__link--number" href="#">Date</a>
						</div>
					</div>
					<% if(transactions != null) { for(TransactionDto transaction:
					transactions) { %> <br>
					<div class="table-content">
						<div class="table-row">
							<div class="table-data"><%=transaction.getId()%></div>
							<div class="table-data"><%=transaction.getAmount()%></div>
							<div class="table-data"><%=transaction.getType()%></div>
							<div class="table-data"><%=transaction.getDate()%></div>
						</div>
					</div>
					<% } } %> <br>
				</div>
			</div>
			<% } } %> 
		</div>
	</div>
	<script type="text/javascript">
		var properties = [ 'ID', 'Amount', 'Type', 'Date', ];

		$.each(properties, function(i, val) {

			var orderClass = '';

			$("#" + val)
					.click(
							function(e) {
								e.preventDefault();
								$('.filter__link.filter__link--active').not(
										this).removeClass(
										'filter__link--active');
								$(this).toggleClass('filter__link--active');
								$('.filter__link').removeClass('asc desc');

								if (orderClass == 'desc' || orderClass == '') {
									$(this).addClass('asc');
									orderClass = 'asc';
								} else {
									$(this).addClass('desc');
									orderClass = 'desc';
								}

								var parent = $(this).closest('.header__item');
								var index = $(".header__item").index(parent);
								var $table = $('.table-content');
								var rows = $table.find('.table-row').get();
								var isSelected = $(this).hasClass(
										'filter__link--active');
								var isNumber = $(this).hasClass(
										'filter__link--number');

								rows.sort(function(a, b) {

									var x = $(a).find('.table-data').eq(index)
											.text();
									var y = $(b).find('.table-data').eq(index)
											.text();

									if (isNumber == true) {

										if (isSelected) {
											return x - y;
										} else {
											return y - x;
										}

									} else {

										if (isSelected) {
											if (x < y)
												return -1;
											if (x > y)
												return 1;
											return 0;
										} else {
											if (x > y)
												return -1;
											if (x < y)
												return 1;
											return 0;
										}
									}
								});

								$.each(rows, function(index, row) {
									$table.append(row);
								});

								return false;
							});

		});
	</script>
	<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/popper.js"></script>
	<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/main.js"></script>
</body>
</html>