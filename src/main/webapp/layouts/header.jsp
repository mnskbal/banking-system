<%@ page language="java" contentType="text/html" %> <%@ page
session="false" %>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<div class="container-fluid">
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="nav navbar-nav ml-auto"
				style="width: 100%; margin-right: auto; margin-left: auto; display: flex; justify-content: center; align-items: center;">
				<li class="nav-item active"
					style="width: 25%; margin-right: auto; margin-left: auto; display: flex; justify-content: center; align-items: center;"><a
					class="nav-link" href="#">Home</a></li>
				<li class="nav-item"
					style="width: 25%; margin-right: auto; margin-left: auto; display: flex; justify-content: center; align-items: center;"><a
					class="nav-link" href="#">About</a></li>
				<li class="nav-item"
					style="width: 25%; margin-right: auto; margin-left: auto; display: flex; justify-content: center; align-items: center;"><a
					class="nav-link" href="#">Portfolio</a></li>
				<li class="nav-item"
					style="width: 25%; margin-right: auto; margin-left: auto; display: flex; justify-content: center; align-items: center;"><a
					class="nav-link" href="#">Contact</a></li>
			</ul>
		</div>
	</div>
</nav>