<%@ page language="java" contentType="text/html" %> <%@ page
session="false" %>
<!doctype html>
<nav id="sidebar">
	<div class="p-4 pt-5">
		<a href="#" class="img logo rounded-circle mb-5"
			style="background-image: url(${pageContext.request.contextPath}/images/logo.jpg);"></a>
		<ul class="list-unstyled components mb-5">
			<li class="active"><a href="/banking-system/api/user/login">Home</a></li>
			<li><a href="/banking-system/api/navigate?action=DEPOSIT&uId=${param.uId}">Deposit</a></li>
			<li><a href="/banking-system/api/navigate?action=WITHDRAW&uId=${param.uId}">Withdraw</a></li>
		</ul>

		<div class="footer">
			<ul class="list-unstyled components mb-5">
				<li class="active"><a
					href="/banking-system/api/user/logout?action=LOG_OUT">Log Out</a></li>
			</ul>
		</div>

	</div>
</nav>